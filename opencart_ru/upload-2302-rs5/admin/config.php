<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8000/admin/');
define('HTTP_CATALOG', 'http://localhost:8000/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8000/admin/');
define('HTTPS_CATALOG', 'http://localhost:8000/');

// DIR
define('DIR_APPLICATION', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/admin/');
define('DIR_SYSTEM', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/');
define('DIR_IMAGE', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/image/');
define('DIR_LANGUAGE', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/admin/language/');
define('DIR_TEMPLATE', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/admin/view/template/');
define('DIR_CONFIG', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/config/');
define('DIR_CACHE', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/storage/download/');
define('DIR_LOGS', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/storage/logs/');
define('DIR_MODIFICATION', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/storage/modification/');
define('DIR_UPLOAD', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/system/storage/upload/');
define('DIR_CATALOG', '/home/sj/PhpstormProjects/shop-shop/opencart_ru/upload-2302-rs5/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
